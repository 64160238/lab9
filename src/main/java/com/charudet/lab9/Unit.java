package com.charudet.lab9;

public class Unit {
	private int x;
	private int y;
	private Map map;
	private boolean dominate;
	private char symbol;

	public Unit(Map map, char symbol, int x, int y, boolean dominate) {
		this.x = x;
		this.y = y;
		this.map = map;
		this.dominate = dominate;
		this.symbol = symbol;
	}

	public static boolean isOn(int x, int y) {
		return x == x && y == y;
	}
	/* การที่จะ setX , setY
	1. อยู่บน map ไหม
	2. ทับตัวที่เป็น dominate อื่นไหม
	*/
	public boolean setX(int x) {
		if(!map.isInWidth(x)) return false;
		if(map.hasDominate(x, y)) return false;
		this.x = x;
		return true;
	}

	public boolean setY(int y) {
		if(!map.isInHeight(y)) return false;
		if(map.hasDominate(x, y)) return false;
		this.y = y;
		return true;
	}

	public boolean setXY(int x, int y) {
		if(!map.isInHeight(y)) return false;
		if(map.hasDominate(x, y)) return false;
		this.x = x;
		this.y = y;
		return true;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isDominate() {
		return dominate;
	}

	public Map getMap() {
		return map;
	}

	public char getSymbol() {
		return symbol;
	}

	public String toStiString() {
		return "Unit(" + this.symbol + ") [" + this.x + ", " + this.y + "] is on " + map; 
	}
}
